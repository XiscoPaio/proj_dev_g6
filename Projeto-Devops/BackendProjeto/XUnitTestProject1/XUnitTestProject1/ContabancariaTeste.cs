using Backend.Models;
using Xunit;

namespace XUnitTestProject1
{
    public class TestesContaBancaria
    {
        private ContaBancaria contaBanc;

        public void Setup()
        {

            contaBanc = (new ContaBancaria
            {
                contaId = 2,
                clienteId = 2,
                cliente = (new Clientes { clienteId = 2, Nome = "Joel", Morada = "Rua Ruah", Contrato = "Global E" }),
                numeroDeConta = 2,
                IBAN = "PT50123456789",
                SaldoCorrente = 2550.50
            });
        }

        [Fact]
        public void Test1()
        {
            var result = contaBanc.contaId;
            Assert.Equal(2, result);
        }

        public void Test2()
        {
            var result = contaBanc.clienteId;
            Assert.Equal(2, result);
        }

        public void Test3()
        {
            var result = contaBanc.IBAN;
            Assert.Equal("PT50123456789", result);
        }

        public void Test4()
        {
            var result = contaBanc.SaldoCorrente;
            Assert.Equal(2550.50, result);
        }


    }
}
