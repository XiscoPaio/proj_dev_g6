﻿using System;
using Backend.Models;
using Microsoft.EntityFrameworkCore;

namespace Backend.Models
{
    class BackendContext : DbContext
    {
        public BackendContext()
        {

        }

        public DbSet<Clientes> Cliente { get; set; }
        public DbSet<ContaBancaria> ContaBancaria { get; set; }
        public DbSet<TipoTransacao> TipoTransacao { get; set; }
        public DbSet<Transacoes> Transacoes { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=WIN-TD9CG9RCF2G;Database=myBD;User Id=sa;Password=Vagrant42;");
        }
    }
}
