﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TipoTransacaoController : ControllerBase
    {
        private readonly List<TipoTransacao> TipoTransacao = new List<TipoTransacao>();


        private readonly ILogger<TipoTransacaoController> _logger;

        public TipoTransacaoController(ILogger<TipoTransacaoController> logger)
        {
            _logger = logger;
            LoadData();

        }

        private void LoadData()
        {
            TipoTransacao.Add(new TipoTransacao { tipoId = 1, tipoTransacao = "Débito" });
            TipoTransacao.Add(new TipoTransacao { tipoId = 2, tipoTransacao = "Crédito" });
        }

        [HttpGet]
        public IEnumerable<TipoTransacao> Get()
        {

            return TipoTransacao;
        }

        [HttpGet("{id}")]
        public ActionResult<TipoTransacao> Get(int id)
        {
            var tipo_transacao = TipoTransacao.FirstOrDefault((p) => p.tipoId == id);

            if (tipo_transacao == null)
            {
                return NotFound();
            }

            return Ok(tipo_transacao);
        }

        [HttpPost]
        public ActionResult Create(TipoTransacao tipo_transacao)
        {
            try
            {
                TipoTransacao.Add(tipo_transacao);
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return Ok(tipo_transacao);

        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, TipoTransacao tipo_transacao)
        {
            if (id != tipo_transacao.tipoId)
            {
                return BadRequest();
            }

            TipoTransacao todoTipoTransacao = TipoTransacao.FirstOrDefault(x => x.tipoId == id);

            if (todoTipoTransacao == null)
            {
                return NotFound();
            }

            todoTipoTransacao.tipoTransacao = tipo_transacao.tipoTransacao;

            return Ok(todoTipoTransacao);
        }

    }
}
