using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ContaBancariaController : ControllerBase
    {
        private readonly List<ContaBancaria> ContaBancaria = new List<ContaBancaria>();

        BackendContext _context = new BackendContext();

        private readonly ILogger<ContaBancariaController> _logger;

        public ContaBancariaController(ILogger<ContaBancariaController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<ContaBancaria> Get()
        {
            var contaBancaria = _context.ContaBancaria.ToList();
            return contaBancaria;
        }

        [HttpGet("{id}")]
        public ActionResult<ContaBancaria> Get(int id)
        {
            var contaBancaria = _context.ContaBancaria.Find(id);

            if (contaBancaria == null)
            {
                return NotFound();
            } else
            {
                ArrayList array = new ArrayList();

                var cliente = _context.Cliente.Find(contaBancaria.clienteId);

                if (cliente == null)
                {
                    return NotFound();
                } else
                {
                    array.Add(cliente.Nome);
                    array.Add(cliente.Morada);
                    array.Add(cliente.Contrato);
                    array.Add(contaBancaria.SaldoCorrente);

                    return Ok(array);
                }
            }

        }

        [HttpPost]
        public ActionResult Create(ContaBancaria contaBancaria)
        {
            try
            {
                ContaBancaria.Add(contaBancaria);
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return Ok(ContaBancaria);

        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, ContaBancaria contaBancaria)
        {
            if (id != contaBancaria.contaId)
            {
                return BadRequest();
            }

            ContaBancaria todoConta = ContaBancaria.FirstOrDefault(x=> x.contaId == id);

            if (todoConta == null)
            {
                return NotFound();
            }

            todoConta.SaldoCorrente = contaBancaria.SaldoCorrente;

            return Ok(todoConta);
        }

    }
}