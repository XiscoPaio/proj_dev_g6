﻿using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClientesController : ControllerBase
    {
        private readonly List<Clientes> Clientes = new List<Clientes>();

        BackendContext _context = new BackendContext();

        private readonly ILogger<ClientesController> _logger;

        public ClientesController(ILogger<ClientesController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Clientes> Get()
        {
            var clientes = _context.Cliente.ToList();
            return clientes;
        }

        [HttpGet("{id}")]
        public ActionResult<Clientes> Get(int id)
        {
            //var cliente = Clientes.FirstOrDefault((p) => p.clienteId == id);
            var cliente = _context.Cliente.Find(id);

            if (cliente == null)
            {
                return NotFound();
            }

            return Ok(cliente);
        }

        [HttpPost]
        public ActionResult Create(Clientes cliente)
        {
            try
            {
                _context.Cliente.Add(cliente);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return Ok(cliente);

        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, Clientes cliente)
        {
            if (id != cliente.clienteId)
            {
                return BadRequest();
            }

            //Clientes todoCliente = Clientes.FirstOrDefault(x=> x.clienteId == id);

            _context.Entry(cliente).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ClientesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        private bool ClientesExists(int id)
        {
            throw new NotImplementedException();
        }
    }
}
