using System;
using System.Collections.Generic;
using System.Linq;
using Backend.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Backend.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TransacoesController : ControllerBase
    {
        private readonly List<Transacoes> Transacoes = new List<Transacoes>();

        BackendContext _context = new BackendContext();

        private readonly ILogger<TransacoesController> _logger;

        public TransacoesController(ILogger<TransacoesController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IEnumerable<Transacoes> Get()
        {
            var transacoes = _context.Transacoes.ToList();
            return transacoes;
        }

        [HttpGet("{id}")]
        public ActionResult<Transacoes> Get(int id)
        {
            var transacao = _context.Transacoes.Find(id);

            if (transacao == null)
            {
                return NotFound();
            }

            return Ok(transacao);
        }

        [HttpPost]
        public ActionResult Create(Transacoes transacao)
        {
            try
            {
                transacao.dia = DateTime.UtcNow;
                transacao.contaBancaria = null;
                transacao.tipoTransacao = null;
                _context.Transacoes.Add(transacao);
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                BadRequest(ex.Message);
            }

            return Ok(transacao);

        }

        [HttpPut("{id}")]
        public ActionResult Update(int id, Transacoes transacao)
        {
            if (id != transacao.transacoesId)
            {
                return BadRequest();
            }

            _context.Entry(transacao).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TransacoesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return NoContent();
        }

        private bool TransacoesExists(int id)
        {
            throw new NotImplementedException();
        }
    }   
}
