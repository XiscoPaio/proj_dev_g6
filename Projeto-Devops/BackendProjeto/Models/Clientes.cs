﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class Clientes
    {
        [Key]
        public int clienteId { get; set; }
        public string Nome { get; set; }
        public string Morada { get; set; }
        public string Contrato { get; set; }
    }
}
