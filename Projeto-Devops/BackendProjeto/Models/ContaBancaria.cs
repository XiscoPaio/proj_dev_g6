using System;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class ContaBancaria
    {
        [Key]
        public int contaId { get; set; }
        [Required]
        public int clienteId { get; set; }
        public Clientes cliente { get; set; }
        public int numeroDeConta { get; set; }
        public string IBAN { get; set; }
        public double SaldoCorrente { get; set; }
        
    }
}