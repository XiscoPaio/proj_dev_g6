﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Backend.Models
{
    public class TipoTransacao
    {
        [Key]
        public int tipoId { get; set; }
        public string tipoTransacao { get; set; }
    }
}   

