using System;
using System.ComponentModel.DataAnnotations;

namespace Backend.Models
{
    public class Transacoes
    {
        [Key]
        public int transacoesId { get; set; }
        [Required]
        public int contaBancariaId { get; set; }
        public ContaBancaria contaBancaria { get; set; }
        public DateTime dia { get; set; }
        public double valor { get; set; }
        [Required]
        public int tipoTransacaoId { get; set; }
        public TipoTransacao tipoTransacao { get; set; }
    }
}
